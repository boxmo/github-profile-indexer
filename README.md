# Desafio Fretadão Fullstack - **Mozart Brum**
Indexador de perfis do Github

# Instalação

## Banco de dados - Postgresql
**Importante** - Necessário versão 10 ou superior para suportar as funções de busca

## Dependências
Ruby versão 3.0.2 e Ruby on Rails versão 6.1.5

Instalando dependências 

```shell
bundle install
```
Criando o banco de dados

```shell
rails db:create

rails db:migrate
```
Instalando os pacotes NPM

```shell
yarn install
```

## Iniciar aplicação 

```shell
./bin/rails s -b 0.0.0.0

# em outro terminal

./bin/webpack-dev-server
```

Ou então inicie a aplicação com [foreman](https://github.com/ddollar/foreman) baseado no Procfile que está na raiz do projeto

instale o [foreman](https://github.com/ddollar/foreman) se já não estiver instalado

```shell
gem install foreman 
```
Inicie a aplicação
```shell
foreman s -p 3000
```

# Testes

Rodando todos os testes

```shell
bundle exec rspec
```

Models 

```shell
bundle exec rspec spec/models
```

Services 

```shell
bundle exec rspec spec/services
```

Features 

```shell
bundle exec rspec spec/features
```

Requests 

```shell
bundle exec rspec spec/requests
```

Views

```shell
bundle exec rspec spec/views
```

# Solução

## Service Objects

A técnica foi utilizada para manter o código mais limpo, com controllers e models enxutos e classes mais fáceis de serem testadas. Os serviços estão na pasta app/services/ e executam o build e update de Models. Outro serviço importante é o de Webscrapper que toma toda a responsabilidade de buscar os dados da página do perfil do GitHub e retornar de forma organizada. 

## Webscrapper

Para buscar os dados na página do GitHub e retirar todos os dados necessários foi utilizada a biblioteca nokogiri. O código pode ser encontrado em app/services/github_user_scrapper.rb.

## Buscas 

As buscas são feitas em todos os campos do perfil utilizando a função nativa de full text search do PostgreSQL. Para isso foi utilizado a gem [pg_search](https://github.com/Casecommons/pg_search) que cria escopos de busca no model.
Foram utilizados dois módulos do postgresql que são instalados automaticamente através de migrations que são eles 
* [**pg_trgm**](https://www.postgresql.org/docs/current/pgtrgm.html) – Permite comparar a similaridade dos textos a partir de grupos de três letras. 
* [**unaccent**](https://www.postgresql.org/docs/current/unaccent.html) – Remove acentos dos textos a serem comparados.

## Encurtamento de URLs

Para a solução de encurtamento das URLs decidi criar uma própria através do model Link onde é salvo a URL do perfil e o slug para identificação do link. On indexamento do slug é feito para que a busca por um link seja feita de forma mais eficiente e para que  garanta que seja único na camada do banco de dados.
Outra feature do encurtamento que foi adicionada mas não foi explorada a fundo é a contagem de clicks, sendo que toda vez em que um link é acessado a contagem é acrecentada em um. 

## Front-end

Para o front-end não foram utilizadas nenhuma biblioteca de estilos. Todo o css foi criado para a aplicação e apresenta de forma responsiva e leve em todas as páginas.

Para as views foi utilizada a template engine slim para acelerar o desenvolvimento por ser uma forma mais enxuta de se escrever o HTML.

No JavaScript foi utilizado framework stimulus. Existe o controller search_controller.js para mostrar os resultados das buscas de perfis enquanto se digita o termo no campo.

## Melhorias

As melhorias que eu considero são as seguintes
* Processar as buscas dos dados do Webscrapper em background utilizando alguma ferramenta como por exemplo Sidekiq para diminuir o tempo da requisição de criação e de atualização do perfil
* Paginação dos perfis retornados na listagem e na busca
* Padronização maior do CSS
