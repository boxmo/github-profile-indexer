class LinksController < ApplicationController 
  
  def show 
    @link = Link.find_by(slug: params[:slug])
    if @link.present?
      @link.increment!(:click_count)
      redirect_to @link.url, status: :moved_permanently
    else
      redirect_to root_path, alert: "Link não encontrado."
    end
  end

end