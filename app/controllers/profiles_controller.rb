class ProfilesController < ApplicationController 
  before_action :set_profile, except: [:index, :new, :create, :search]

  def index 
    @profiles = Profile
      .includes(:link)
      .order(created_at: :desc)
  end

  def new
    @profile = ProfileBuilder.call
  end

  def edit; end;
  def show; end;

  def create 
    @profile = ProfileBuilder.call(profile_params)
    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Perfil criado com sucesso.' }
      else
        flash.now.alert = "Perfil não pode ser criado."
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def update 
    respond_to do |format|
      if ProfileUpdater.call(@profile, profile_params)
        format.html { redirect_to @profile, notice: 'Perfil atualizado com sucesso.' }
      else
        flash.now.alert = "Perfil não pode ser atualizado."
        format.html { render :edit, status: :unprocessable_entity }
      end  
    end
  end

  def destroy
    respond_to do |format|
      if @profile.destroy
        format.html { redirect_to profiles_path, notice: 'Perfil excluído com sucesso.' }
      else
        format.html { redirect_to profile_path(@profile), alert: "Perfil não pode ser excluído.", status: :unprocessable_entity }
      end
    end
  end

  def search 
    respond_to do |format|
      if params[:q].present?
        @profiles = Profile.includes(:link).search_all(params[:q])
        format.html { render :index }
        format.json { render json: @profiles }
      else        
        format.html do 
          @profiles = Profile.includes(:link).order(created_at: :desc)
          render :index
        end           
        format.json { render json: [] }
      end
    end
  end

  def rescan 
    gh_data = GithubUserScrapper.new(@profile.link.url).scrap 
    if @profile.update(gh_data)
      redirect_to @profile, notice: 'Perfil atualizado com sucesso.'
    else
      redirect_to @profile, alert: "Perfil não pode ser atualizado.", status: :unprocessable_entity
    end
  end


  private 

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params 
    params.require(:profile).permit(
      :name, 
      link_attributes: [:id, :url]
    )
  end

end