import {
  ApplicationController,
  useDebounce,
  useClickOutside
} from 'stimulus-use'


export default class extends ApplicationController {
  static debounces = ['perform']
  static targets = ['input', 'list', 'spinner']

  connect() {
    useDebounce(this, { wait: 300 })
    useClickOutside(this)
  }

  async perform() {
    const query = this.inputTarget.value

    if (query.length < 2) {
      this.clearList()
      return
    }

    const response = await fetch(`/profiles/search?q=${query}`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }
    )
    const json = await response.json()
    if (json.length) this.showResults(json)
  }


  showResults(results) {
    this.clearList()
    results.forEach(result => {
      const link = this.buildResultElement(result)
      this.listTarget.appendChild(link)
    })
    this.listTarget.classList.remove('hidden')
  }

  clickOutside(event) {
    this.clearList()
  }

  clearList() {
    this.listTarget.innerHTML = ''
    this.listTarget.classList.add('hidden')
  }

  buildResultElement(data) {
    const link = document.createElement('a')
    const img = document.createElement('img')
    const span = document.createElement('span')

    link.classList.add('result')
    link.href = `/profiles/${data.id}`

    img.src = data.gh_avatar_url
    img.classList.add('profile-img')
    img.style.width = '50px'

    span.textContent = data.name

    link.appendChild(img)
    link.appendChild(span)
    return link
  }

}
