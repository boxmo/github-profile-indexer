# == Schema Information
#
# Table name: links
#
#  id          :bigint           not null, primary key
#  profile_id  :bigint           not null
#  url         :string           not null
#  slug        :string           not null
#  click_count :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Link < ApplicationRecord
  belongs_to :profile

  validates :url, presence: true, github_url: true
  validates :slug, presence: true, uniqueness: true

  def to_param
    slug
  end
  
end
