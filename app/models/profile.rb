# == Schema Information
#
# Table name: profiles
#
#  id                      :bigint           not null, primary key
#  name                    :string           not null
#  gh_username             :string           not null
#  gh_name                 :string           not null
#  gh_followers            :string           not null
#  gh_following            :string           not null
#  gh_stars                :string           not null
#  gh_yearly_contributions :string           not null
#  gh_avatar_url           :string           not null
#  gh_organization         :string
#  gh_location             :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
class Profile < ApplicationRecord
  include PgSearch::Model

  has_one :link, dependent: :destroy
  
  pg_search_scope :search_all, against: [
    :name, 
    :gh_name,
    :gh_username,
    :gh_stars,
    :gh_followers,
    :gh_following,
    :gh_yearly_contributions,
    :gh_organization,
    :gh_location
  ], 
  using: { trigram: { word_similarity: true } },
  ignoring: :accents

  validates :name, presence: true
  validates :gh_name, presence: true
  validates :gh_stars, presence: true
  validates :gh_username, presence: true
  validates :gh_followers, presence: true
  validates :gh_following, presence: true
  validates :gh_avatar_url, presence: true
  validates :gh_yearly_contributions, presence: true
  validates :link, presence: true


  accepts_nested_attributes_for :link
end
