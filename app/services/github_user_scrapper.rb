require 'nokogiri'
require 'open-uri'

class GithubUserScrapper

  TEXT_EXTRACTORS = {
    gh_name:         ".vcard-names .p-name",
    gh_username:     ".vcard-names .p-nickname",
    gh_stars:        ".UnderlineNav:not(.d-md-none) [aria-label='User profile'] [data-tab-item='stars'] .Counter",
    gh_followers:    "a[href$='?tab=followers'] span",
    gh_following:    "a[href$='?tab=following'] span",
    gh_organization: "li[itemprop='worksFor'] .p-org div",
    gh_location:     "li[itemprop='homeLocation'] .p-label"
  }.freeze

  IMAGE_EXTRACTORS = {
    gh_avatar_url: "img.avatar-user.avatar.width-full"
  }.freeze

  NUMBER_EXTRACTORS = {
    gh_yearly_contributions: ".js-yearly-contributions h2"
  }.freeze

  def initialize url
    if valid_url? url
      @url = url
      @doc = Nokogiri::HTML URI.open(@url)
    end
  rescue OpenURI::HTTPError => e
    @doc = nil
  end

  def scrap 
    return nullify_attributes unless @doc
    scrap_text   = Hash[TEXT_EXTRACTORS.map   { |k, v| [k, text_parser(v)]   }]
    scrap_image  = Hash[IMAGE_EXTRACTORS.map  { |k, v| [k, image_parser(v)]  }]
    scrap_number = Hash[NUMBER_EXTRACTORS.map { |k, v| [k, number_parser(v)] }]
    Hash.new.merge(scrap_text, scrap_image, scrap_number)
  end

  private 

  def nullify_attributes
    [TEXT_EXTRACTORS, IMAGE_EXTRACTORS, NUMBER_EXTRACTORS]
      .map { |extractor| Hash[extractor.map { |k, v| [k, nil] }] }
      .reduce(:merge)
  end

  def valid_url? url
    url.present? && url.match(/https:\/\/github\.com\/.+/)
  end

  def parser selector
    @doc.css(selector)
  end

  def text_parser selector 
    css_parser = parser(selector)
    return nil unless css_parser.present?
    css_parser.text.strip
  end

  def image_parser selector
    css_parser = parser(selector)
    return nil unless css_parser.present?
    css_parser.attr('src').value.strip
  end

  def number_parser selector
    css_parser = parser(selector)
    return nil unless css_parser.present?
    css_parser.text.scan(/\d/).join('').strip
  end

end




