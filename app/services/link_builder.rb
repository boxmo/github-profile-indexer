class LinkBuilder
  attr_reader :link

  def initialize params
    @link = Link.new params
  end

  def self.call params = {}
    self.new(params).tap { |builder| builder.build }.link
  end

  def build 
    set_slug
  end

  private 

  def set_slug
    @link.slug = loop do 
      slug = SecureRandom.urlsafe_base64 3
      break slug unless Link.where(slug: slug).exists?
    end
  end

end