class ProfileBuilder
  attr_reader :profile

  def initialize params 
    @profile = Profile.new(params)
  end

  def self.call params = {}
    self.new(params).tap { |builder| builder.build(params) }.profile
  end

  def build params
    set_link(params[:link_attributes])
    set_scrap_data(@profile.link.url)
  end

  private 

  def set_link link_attributes
    @profile.link = LinkBuilder.call(link_attributes)
  end

  def set_scrap_data url
    @profile.assign_attributes(
      GithubUserScrapper.new(url).scrap 
    )
  end

end