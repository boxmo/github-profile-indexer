class ProfileUpdater

  def initialize profile, params 
    @profile = profile 
    @params = params
  end

  def self.call profile, params 
    self.new(profile, params).update
  end

  def update
    @profile.assign_attributes(@params)
    set_scrap_data(@profile.link.url)
    @profile.save
  end

  private 

  def set_scrap_data url
    @profile.assign_attributes(
      GithubUserScrapper.new(url).scrap 
    )
  end

end