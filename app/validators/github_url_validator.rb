require 'uri'

class GithubUrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /^#{URI::regexp}$/ && value =~ /^https:\/\/github\.com\/.+$/
      record.errors.add(attribute, options[:message] || "não é uma URL do github válida")
    end
  end
end