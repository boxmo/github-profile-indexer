class CreateProfiles < ActiveRecord::Migration[6.1]
  def change
    create_table :profiles do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.string :gh_username, null: false
      t.string :gh_name, null: false
      t.string :gh_followers, null: false
      t.string :gh_following, null: false
      t.string :gh_stars, null: false
      t.string :gh_yearly_contributions, null: false
      t.string :gh_avatar_url, null: false
      t.string :gh_organization
      t.string :gh_location
      t.timestamps
    end
  end
end
