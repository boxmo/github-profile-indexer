class CreateLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :links do |t|
      t.references :profile, null: false, foreign_key: true
      t.string :url, null: false
      t.string :slug, null: false
      t.integer :click_count, default: 0

      t.timestamps
    end
    add_index :links, :slug, unique: true
  end
end
