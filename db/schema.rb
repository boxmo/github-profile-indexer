# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_27_224555) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "links", force: :cascade do |t|
    t.bigint "profile_id", null: false
    t.string "url", null: false
    t.string "slug", null: false
    t.integer "click_count", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_links_on_profile_id"
    t.index ["slug"], name: "index_links_on_slug", unique: true
  end

  create_table "profiles", force: :cascade do |t|
    t.string "name", null: false
    t.string "gh_username", null: false
    t.string "gh_name", null: false
    t.string "gh_followers", null: false
    t.string "gh_following", null: false
    t.string "gh_stars", null: false
    t.string "gh_yearly_contributions", null: false
    t.string "gh_avatar_url", null: false
    t.string "gh_organization"
    t.string "gh_location"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "links", "profiles"
end
