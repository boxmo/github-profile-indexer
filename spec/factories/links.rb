# == Schema Information
#
# Table name: links
#
#  id          :bigint           not null, primary key
#  profile_id  :bigint           not null
#  url         :string           not null
#  slug        :string           not null
#  click_count :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :link do
    url { "https://github.com/matz" }
    slug { SecureRandom.urlsafe_base64 3 }
    click_count { 0 }
    profile
  end
end
