# == Schema Information
#
# Table name: profiles
#
#  id                      :bigint           not null, primary key
#  name                    :string           not null
#  gh_username             :string           not null
#  gh_name                 :string           not null
#  gh_followers            :string           not null
#  gh_following            :string           not null
#  gh_stars                :string           not null
#  gh_yearly_contributions :string           not null
#  gh_avatar_url           :string           not null
#  gh_organization         :string
#  gh_location             :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
FactoryBot.define do
  factory :profile do
    name { "matz" } 
    gh_username { "matz" } 
    gh_name { "Yukihiro Matz Matsumoto" }
    gh_followers { "8.7k" }
    gh_following { "1" } 
    gh_stars { "10" } 
    gh_yearly_contributions { "927" } 
    gh_avatar_url { "https://avatars.githubusercontent.com/u/30733?v=4" }
    gh_organization { "Ruby Association,NaCl" }
    gh_location { "Matsue, Japan" }

    before(:create) { |profile| build(:link, profile: profile) }
    after(:build) { |profile| build(:link, profile: profile) }
  end
end
