require "rails_helper"

RSpec.feature "Creating Profile", type: :feature do
  
  before(:each) do
    visit "/profiles/new"
  end

  scenario "valid inputs" do

    fill_in "profile[name]", with: "matz"
    fill_in "profile[link_attributes][url]", with: "https://github.com/matz"

    click_button "Salvar"

    expect(page).to have_text("Perfil criado com sucesso.")
    expect(page).to have_text("matz")

    visit profiles_path 

    expect(page).to have_text("matz")
  end

  scenario "invalid name" do 
    fill_in "profile[link_attributes][url]", with: "https://github.com/matz"
    click_button "Salvar"
    expect(page).to have_text("Perfil não pode ser criado.")
    expect(page).to have_text("Nome não pode ficar em branco")
  end

  scenario "invalid url" do 
    fill_in "profile[name]", with: "matz"
    click_button "Salvar"
    expect(page).to have_text("Perfil não pode ser criado.")
    expect(page).to have_text("URL do Github não pode ficar em branco")
    expect(page).to have_text("URL do Github não é uma URL do github válida")
  end



end