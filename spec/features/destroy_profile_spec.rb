require "rails_helper"

RSpec.feature "Destroy Profile", type: :feature do
  
  scenario "success" do
    profile = create :profile
    visit profile_path profile
    click_link "Excluir"
    expect(page).to have_text("Perfil excluído com sucesso.")
    expect(Profile.count).to eq 0
  end

end