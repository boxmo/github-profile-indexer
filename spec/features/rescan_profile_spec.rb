require "rails_helper"

RSpec.feature "Rescan Profile", type: :feature do
  
  scenario "success" do
    profile = create :profile
    visit profile_path profile
    click_link "Re-escanear"
    expect(page).to have_text("Perfil atualizado com sucesso.")
  end

end