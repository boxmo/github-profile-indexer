require "rails_helper"

RSpec.feature "Search Profile", type: :feature do
  
  before(:each) do
    @profile = create :profile
  end

  scenario "found" do
    visit root_path
    fill_in "q", with: "matz"
    click_button "search"
    expect(page).to have_text("matz")
  end

  scenario "not found" do
    visit root_path
    fill_in "q", with: "boxmo"
    click_button "search"
    expect(page).to have_text("Nenhum perfil encontrado")
  end

end