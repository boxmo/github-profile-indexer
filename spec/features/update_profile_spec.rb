require "rails_helper"

RSpec.feature "Updating Profile", type: :feature do
  
  before(:each) do
    @profile = create :profile
  end

  scenario "valid inputs" do
    visit edit_profile_path(@profile)

    fill_in "profile[name]", with: "boxmo"

    click_button "Salvar"

    expect(page).to have_text("Perfil atualizado com sucesso.")
    expect(page).to have_text("boxmo")

    visit profiles_path 

    expect(page).to have_text("boxmo")
  end

  scenario "invalid name" do 
    visit edit_profile_path(@profile)
    fill_in "profile[name]", with: ""
    click_button "Salvar"
    expect(page).to have_text("Perfil não pode ser atualizado.")
    expect(page).to have_text("Nome não pode ficar em branco")
  end

  scenario "invalid url" do 
    visit edit_profile_path(@profile)
    fill_in "profile[link_attributes][url]", with: ""
    click_button "Salvar"
    expect(page).to have_text("Perfil não pode ser atualizado.")
    expect(page).to have_text("URL do Github não pode ficar em branco")
    expect(page).to have_text("URL do Github não é uma URL do github válida")
  end



end