# == Schema Information
#
# Table name: links
#
#  id          :bigint           not null, primary key
#  profile_id  :bigint           not null
#  url         :string           not null
#  slug        :string           not null
#  click_count :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe Link, type: :model do

  describe "Validations" do
      

    it "is valid with valid attributes" do
      link = build :link
      expect(link).to be_valid
    end

    it 'is not valid without a url' do
      link = build :link, url: nil
      expect(link).to_not be_valid
    end

    it 'is not valid without a profile' do
      link = build :link, profile: nil
      expect(link).to_not be_valid
    end

    it 'is not valid without a slug' do
      link = build :link, slug: nil
      expect(link).to_not be_valid
    end

    it 'is not valid without a valid github url' do
      link = build :link, url: "https://google.com"
      expect(link).to_not be_valid
    end

    it 'is not valid without a unique slug' do
      link1 = create :link
      link2 = build :link, slug: link1.slug
      expect(link2).to_not be_valid
    end

  end

end
