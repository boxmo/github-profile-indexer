# == Schema Information
#
# Table name: profiles
#
#  id                      :bigint           not null, primary key
#  name                    :string           not null
#  gh_username             :string           not null
#  gh_name                 :string           not null
#  gh_followers            :string           not null
#  gh_following            :string           not null
#  gh_stars                :string           not null
#  gh_yearly_contributions :string           not null
#  gh_avatar_url           :string           not null
#  gh_organization         :string
#  gh_location             :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
require 'rails_helper'

RSpec.describe Profile, type: :model do

  describe "Validations" do
    
    it "is valid with valid attributes" do
      profile = build(:profile)
      expect(profile).to be_valid
    end
  
    it "is valid without github organization" do
      profile = build(:profile, gh_organization: nil)
      expect(profile).to be_valid
    end
  
    it "is valid without github location" do
      profile = build(:profile, gh_location: nil)
      expect(profile).to be_valid
    end
  
    it "is not valid without a name" do
      profile = build(:profile, name: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github username" do
      profile = build(:profile, gh_username: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github name" do
      profile = build(:profile, gh_name: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github followers" do
      profile = build(:profile, gh_followers: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github following" do
      profile = build(:profile, gh_following: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github stars" do
      profile = build(:profile, gh_stars: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github yearly contributions" do
      profile = build(:profile, gh_yearly_contributions: nil)
      expect(profile).to_not be_valid
    end
  
    it "is not valid without github avatar url" do
      profile = build(:profile, gh_avatar_url: nil)
      expect(profile).to_not be_valid
    end
  
  end

  describe "Search" do
    before(:each) do
      @matz = create(:profile)
      @boxmo = create(:profile, 
        name: "Mozart Brum",
        gh_username: "boxmo", 
        gh_name:'Mozart Brum',
        gh_followers: "36",
        gh_following: "66",
        gh_stars: "1.2k", 
        gh_yearly_contributions: "314",
        gh_organization: "casare.me", 
        gh_location: "São Paulo - SP" 
      )
    end
    
    it "finds a profile by name" do
      expect(described_class.search_all "matz").to match_array [@matz]
      expect(described_class.search_all "Mozart").to match_array [@boxmo]
      expect(described_class.search_all "Brum").to match_array [@boxmo]

    end
    
    it "finds a profile by github username" do
      expect(described_class.search_all "matz").to match_array [@matz]
      expect(described_class.search_all "boxmo").to match_array [@boxmo]
    end

    it "finds a profile by github name" do
      expect(described_class.search_all 'Yukihiro Matz Matsumoto').to match_array [@matz]
      expect(described_class.search_all 'Yukihiro').to match_array [@matz]
      expect(described_class.search_all "Mozart").to match_array [@boxmo]
      expect(described_class.search_all "Brum").to match_array [@boxmo]
    end

    it "finds a profile by github stars" do
      expect(described_class.search_all "10").to match_array [@matz]
      expect(described_class.search_all "1.2").to match_array [@boxmo]
      expect(described_class.search_all "1").to match_array [@matz, @boxmo]
    end

    it "finds a profile by github followers" do
      expect(described_class.search_all "8.7k").to match_array [@matz]
      expect(described_class.search_all "36").to match_array [@boxmo]
    end

    it "finds a profile by github following" do
      expect(described_class.search_all "66").to match_array [@boxmo]
    end

    it "finds a profile by github yearly contributions" do
      expect(described_class.search_all "927").to match_array [@matz]
      expect(described_class.search_all "314").to match_array [@boxmo]
    end

    it "finds a profile by github organization" do
      expect(described_class.search_all "ruby association").to match_array [@matz]
      expect(described_class.search_all "casare").to match_array [@boxmo]
    end

    it "finds a profile by github location" do
      expect(described_class.search_all "Matsue, Japan").to match_array [@matz]
      expect(described_class.search_all "são paulo").to match_array [@boxmo]
    end


  end

end
