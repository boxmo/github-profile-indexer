require 'rails_helper'

RSpec.describe "Links", type: :request do

  it 'redirects' do  
    @link = create :link
    get link_path(@link)

    expect(response).to redirect_to @link.url
  end

end
