require 'rails_helper'

RSpec.describe "Profiles", type: :request do

  it 'list all' do
    create(:profile, name: "Matz")
    get profiles_path
    expect(response).to be_successful
    expect(response.body).to include "Matz"
  end

  it 'search for' do 
    create(:profile, name: "John")
    create(:profile, name: "Jack")

    get search_profiles_path, params: { q: "John" }

    expect(response).to be_successful
    expect(response.body).to include "John"
    expect(response.body).not_to include "Jack"

    get search_profiles_path, params: { q: "" }

    expect(response.body).to include "John"
    expect(response.body).to include "Jack"

  end

  it 'creates' do
    post profiles_path, params: { profile: { name: "Boxmo", link_attributes: { url: "https://github.com/boxmo" }  } }

    expect(response).to redirect_to profile_path(Profile.last)

    get profile_path Profile.last
    expect(response.body).to include "Boxmo"    

  end


  it 'updates' do
    @profile = create :profile, name: "John"
    patch profile_path(@profile.id), params: { profile: { name: "Jack"  } }

    expect(response).to redirect_to @profile

    get profile_path @profile

    expect(response.body).to include "Jack" 
  end

  it 'destroys' do
    @profile = create :profile 
    delete profile_path(@profile.id)

    expect(response).to redirect_to profiles_path

    expect(Profile.count).to eq 0

  end

  it 'rescans' do
    @profile = create :profile
    
    post rescan_profile_path(@profile.id)

    expect(response).to redirect_to @profile
    
  end


end