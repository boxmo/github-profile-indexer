require 'rails_helper'

RSpec.describe GithubUserScrapper, type: :model do

  describe 'with valid url' do
    it 'return github attributes' do
      url = "https://github.com/boxmo"
      data = GithubUserScrapper.new(url).scrap

      expect(data[:gh_username]).to eq "boxmo"
    end
  end

  describe 'with invalid url' do
    it 'return nil attributes' do
      url = "https://github.com"
      data = GithubUserScrapper.new(url).scrap

      expect(data[:gh_username]).to be_nil
    end
  end

end