require 'rails_helper'

RSpec.describe LinkBuilder, type: :model do

  describe 'build link' do
   
    it 'generates slug' do
      link = LinkBuilder.call
      expect(link.slug).not_to be_empty
    end

  end

end