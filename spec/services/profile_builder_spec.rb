require 'rails_helper'

RSpec.describe ProfileBuilder, type: :model do

  describe 'build profile' do
    let(:data) {{name: "matz", link_attributes: { url: "https://github.com/matz" } } }
    let(:invalid_gh_url) { "https://github.com" }
    
    it 'return a valid profile when all attributes are valid' do
      profile = ProfileBuilder.call data
      expect(profile).to be_valid
    end

    it 'return an invalid profile when name is nil' do
      profile = ProfileBuilder.call data.merge(name: nil)
      expect(profile).to_not be_valid
    end

    it 'return an invalid profile when url is nil' do
      profile = ProfileBuilder.call data.merge(link_attributes: { url: nil })
      expect(profile).to_not be_valid
    end

    it 'validate github url format' do
      profile = ProfileBuilder.call data.merge(link_attributes: { url: invalid_gh_url })
      expect(profile).to_not be_valid
    end

  end

end