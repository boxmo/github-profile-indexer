require 'rails_helper'

RSpec.describe ProfileUpdater, type: :model do

  let(:data) { { name: "boxmo", link_attributes: { url: "https://github.com/boxmo" } } }
  let(:invalid_gh_url) { "https://github.com" }

  before(:each) do
    @profile = create :profile
  end

  it 'update when all attributes are valid' do
    ProfileUpdater.call @profile, data
    expect(@profile.name).to eq 'boxmo'
    expect(@profile.gh_name).to eq 'Mozart Brum'
    expect(@profile.gh_username).to eq 'boxmo'
  end

  it 'update with only name attribute' do
    response = ProfileUpdater.call @profile, { name: "boxmo" }
    expect(response).to be true
    expect(@profile.name).to eq 'boxmo'
  end

  it 'update with only link url' do
    response = ProfileUpdater.call @profile, data.merge(link_attributes: { id: @profile.link.id, url: "https://github.com/boxmo" })
    expect(response).to be true
    expect(@profile.link.url).to eq "https://github.com/boxmo"
  end

  it 'do not update  when name is not valid' do
    response = ProfileUpdater.call @profile, data.merge(name: nil)
    expect(response).to be false
  end

  it 'do not update  when link url is not valid' do
    response = ProfileUpdater.call @profile, data.merge(link_attributes: { id: @profile.link.id, url: invalid_gh_url })
    expect(response).to be false
  end


end