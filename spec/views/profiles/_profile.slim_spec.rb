require "rails_helper"

RSpec.describe "profiles/_profile", type: :view do
  before(:each) do
    @profile = create :profile
  end

  it "render a profile" do
    render partial: "profiles/profile", locals: { profile: @profile }

    expect(rendered).to have_css("img.profile-img[src*='#{@profile.gh_avatar_url}']")
    expect(rendered).to have_selector(".profile-main", text: "matz")
    expect(rendered).to have_selector(".profile-link", text: link_url(@profile.link))

    expect(rendered).to have_link(href: link_url(@profile.link))
    expect(rendered).to have_link(href: edit_profile_path(@profile))
    expect(rendered).to have_link(href: profile_path(@profile)) 

  end
end