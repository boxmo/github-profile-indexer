require "rails_helper"

RSpec.describe "profiles/show", type: :view do
  before(:each) do
    @profile = create :profile
  end

  it "renders a profile" do
    render

    expect(rendered).to have_css("img.profile-img[src*='#{@profile.gh_avatar_url}']")
    expect(rendered).to have_selector(".profile-main", text: "matz")
    expect(rendered).to have_selector(".profile-username", text: "matz")
    
    expect(rendered).to have_selector(".stat", text: "8.7k")
    expect(rendered).to have_selector(".stat", text: "1")
    expect(rendered).to have_selector(".stat", text: "10")

    expect(rendered).to have_selector(".info", text: "Ruby Association,NaCl")
    expect(rendered).to have_selector(".info", text: "Matsue, Japan")
    expect(rendered).to have_selector(".info", text: "927")

    expect(rendered).to have_link(href: rescan_profile_path(@profile))
    expect(rendered).to have_link(href: edit_profile_path(@profile))
    expect(rendered).to have_link(href: profile_path(@profile)) { |link| link["data-method"] == "delete" }


  end
end